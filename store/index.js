import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      posts: [
        {
          date: '07/ago',
          title: 'Redação da PR',
          slug: 'redacao-pr',
          content: `
          Durante essa semana, eu fui atrás da produção da proposta.
          <br>
          Para conseguir um produto bom, tive uma reunião com o coordenador e com o orientador, o Renato e o prof. Alfredo, 
          para discutir o escopo da pesquisa. Ficou acertado que será uma pesquisa voltada a um caso específico:
          <em>implementar desenvolvimento em produção com entrega num cluster Kubernetes com auxílio do Istio para o Hacknizer, um sistema de organização e apoio de hackathons</em>
          `
        },
        {
          date: '16/ago',
          title: 'Correção da PR',
          slug: 'correcao-pr',
          content: `
          Nesta semana, eu tive o feedback da primeira versão da PR e trabalhei na correção do que foi apontado.
          <br>
          O que precisava ser corrigido era que a distinção entre <em>pesquisa</em> e <em>implementação</em> não tinha ficado muito clara.
          Notei que não havia sido dito que a ideia é seguir um padrão incremental de desenvolvimento do trabalho, no qual as fases citadas não são cronologicamente muito distintas. Ao longo do semestre, pretendo fazer cada uma das 4 fases nesse estilo, no qual, a cada nova descoberta na pesquisa, eu possa fazer uma pequena implementação.
          `
        },
        {
          date: '25/ago',
          title: 'Consolidação de k8s',
          slug: 'consolidacao-k8s',
          content: `
          Nesta semana, eu comecei a consolidar definições e conceitos sobre kubernetes (k8s).
          <br>
          Estou acompanhando um <a href="https://www.udemy.com/kubernetes-from-a-devops-kubernetes-guru/learn/v4/overview">curso na Udemy sobre Kubernetes</a>. A ideia é finalizá-lo o quanto antes, para poder focar nas demais ferramentas, como Helm, Istio e Telepresence.
          `
        },
        {
          date: '1/set',
          title: 'Prática no tutorial oficial de k8s',
          slug: 'tutorial-k8s',
          content: `
          Após ter feito um estudo teórico sobre k8s, resolvi ir atrás de praticar com o tutorial oficial do k8s.
          <br>
          O tutorial oferece uma ferramenta interativa acompanhada da documentação relacionada. É realmente bem claro e bom, mas ainda não consegui chegar ao final.
          `
        },
        {
          date: '8/set',
          title: 'Entendendo o Helm-Chart',
          slug: 'entendendo-helm-chart',
          content: `
          Finalizei o tutorial oficial de k8s, não é tão extenso quanto poderia ser, mas é bastante completo e interessante. Além disso, fui atrás de informações teóricas sobre Helm-Chart.
          <br>
          Encontrei alguns tutoriais de Helm, mas queria entender um pouco mais sobre a teoria por trás. Ele basea-se em "templatizar" arquivos de configuração do k8s e centralizar dados em um outro arquivo, o que facilita a criação de um deploy mais completo para um sistema mais complexo.
          `
        },
        {
          date: '15/set',
          title: 'Praticando com Helm-Chart',
          slug: 'praticando-helm-chart',
          content: `
          Comecei a fazer alguns testes com o Helm-Chart.
          <br>
          Para praticar um pouco o Helm, criei uma aplicação "toy" para brincar com o chart e consegui entender e articular o que tinha lido a respeito. Agora estou rabiscando algumas coisas para aplicar o Helm-Chart ao Hacknizer. Assim que eu conseguir implementar o chart, tentarei criar uma pipeline no gitlab para automatizar o processo.
          `
        },
      ]
    }
  })
}

export default createStore
